import { NextFunction, Request, Response } from 'express';

export class IndexController {
    public index(req: Request, res: Response, next: NextFunction) {
        res.send('Hello World!');
    }
    public about(req: Request, res: Response, next: NextFunction) {
        res.send('About');
    }
}
