import { Express, Router } from 'express';
import { IndexController } from '@controllers/index.controller';
import { RouteInterface } from '@interfaces/route.interface';

export class IndexRoute implements RouteInterface {
    indexController: IndexController = new IndexController();
    path: string = '/';
    router: Router = Router();
    constructor() {
        this.initializeRoutes();
    }

    private initializeRoutes() {
        this.router.get(`${this.path}`, this.indexController.index);
        this.router.get(`${this.path}about`, this.indexController.about);
    }
}
